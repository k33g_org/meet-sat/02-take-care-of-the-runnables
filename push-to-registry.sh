# Change the GitLab project ID
project_id="33178966"
wasmpath="hello" # Runnable project path
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --upload-file ${wasmpath}/${wasmfile} \
     "https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}"
