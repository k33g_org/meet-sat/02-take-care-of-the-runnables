# 02-take-care-of-the-runnables

> Suborbital CLI and Sat are installed at the startup

## Create a Runnable with Subo

```bash
subo create runnable hello
```
> This command will generate a Rust project, have a look 👀 to the source code: `./hello/src/lib.rs`

```bash
subo build ./hello
```
> This 2nd command will build the wasm file: `./hello/hello.wasm`

## Push the WebAssembly function (Runnable) to the GitLab Package Registry

```bash
# Change the GitLab project ID
project_id="33178966"
wasmpath="hello" # Runnable project path
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --upload-file ${wasmpath}/${wasmfile} \
     "https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}"
```

## Serve the WebAssembly function (Runnable) from the registry

```bash
#!/bin/bash
# Change the GitLab project ID
project_id="33178966"
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"

SAT_HTTP_PORT=8080 sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}

```

Logs:
```bash
{"log_message":"(W) configured to use HTTP with no TLS","timestamp":"2022-01-26T06:43:36.841357919Z","level":2}
{"log_message":"(I) starting hello ...","timestamp":"2022-01-26T06:43:36.841890307Z","level":3}
{"log_message":"(I) serving on :8080","timestamp":"2022-01-26T06:43:36.841961977Z","level":3}
{"log_message":"(I) [discovery-local] starting discovery, advertising endpoint 8080 /meta/message","timestamp":"2022-01-26T06:43:36.841951464Z","level":3}
```

## Call the function

```bash
curl localhost:8080 -d 'Bob Morane'
# response: Hello Bob Morane
```

